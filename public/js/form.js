$(function() {

    $('#form').validate({
        rules: {
            email:{
                required: true,
                email: true
            },
            name: "required",
            phone: "required",

        }
    });

    $('#form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this),
            method = form.attr('method'),
            name = form.find('#name').val(),
            email = form.find('#email').val(),
            phone = form.find('#phone').val(),
            pageEmail = form.find('#pageEmail').val(),
            data = {
                name: name,
                email: email,
                phone: phone,
                pageEmail: pageEmail
            };



        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#csrf').val()
            }
        });
        $.ajax({
            url: form.attr('action'),
            type: method,
            data: data,
            dataType: 'json',
            success: function() {
                alert('success');
            }
        });

        $('form[id=comments]').trigger('reset');

    });
});