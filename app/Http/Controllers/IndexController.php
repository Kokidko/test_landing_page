<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Illuminate\Support\Facades\Mail;


class IndexController extends Controller
{


    public function execute(){
        $pages = Page::all();
        return view('main_content')->with(compact('pages'));
    }

    public function send(Request $request){

            $this->validate($request, [
                'name' => 'required|min:2',
                'email' => 'required|email',
                'phone' => 'required|numeric'
            ]);

            $data = $request->all();
            Mail::send('email', ['data'=>$data], function($message) use($data){
                $message->from($data['email'],$data['name']);
                $message->to($data['pageEmail'])->subject('Question');
            });


    }
}
