<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    public function execute(Page $page){

        return view('page')->with(compact('page'));

    }
}
