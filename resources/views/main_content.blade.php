@extends('index')

@section('header')
    @include('header')
@endsection

@section('main')
@if(isset($pages))
    @foreach($pages as $key => $page)
        @if($key%2 == 0)
            <!--Hero_Section-->
            <section id="hero_section" class="top_cont_outer">
                <div class="hero_wrapper">
                    <div class="container">
                        <div class="hero_section">
                            <div class="row">
                                <div class="col-lg-5 col-sm-7">
                                    <div class="top_left_cont zoomIn wow animated">
                                        {!! $page->description !!}
                                        <div class="work_bottom">

                                            <span><a href="page/{{$page->slug}}" class="contact_btn">Read more</a>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-lg-7 col-sm-5">
                                    <img src="assets/img/{{$page->image}}" class="zoomIn wow animated" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Hero_Section-->
        @else
            <!--Aboutus-->
            <section id="aboutUs">
                <div class="inner_wrapper">
                    <div class="container">
                        <h2>{{$page->title}}</h2>
                        <div class="inner_section">
                            <div class="row">
                                <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right"><img src="assets/img/{{$page->image}}" class="img-circle delay-03s animated wow zoomIn" alt=""></div>
                                <div class=" col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-left">
                                    <div class=" delay-01s animated fadeInDown wow animated">
                                        {!! $page->description !!}
                                    </div>
                                    <div class="work_bottom">

                                        <span>Want to know more..</span> <a href="page/{{$page->slug}}" class="contact_btn">Contact Us</a>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </section>
            <!--Aboutus-->
        @endif
    @endforeach
@endif
@endsection('main')


