@extends('index')

@section('header')
    <header id="header_wrapper">
        <div class="container">
            <div class="header_box">
                <div class="logo">
                        <img src="{{ asset('assets/img/logo.png') }}" alt="logo">
                </div>
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                            <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="main-nav" class="collapse navbar-collapse navStyle">
                        <ul class="nav navbar-nav" id="mainNav">
                                <li><a href="/" class="scroll-link">Back</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
@endsection

@section('main')
<section>
    <div class="inner_section">
        <div class="inner_wrapper">
            <div class="container">
                <h2>{{$page->title}}</h2>
                <div class="inner_section">
                    <div class="row">
                        <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right">
                            <img src="../assets/img/{{$page->image}}" class="img-circle delay-03s animated wow zoomIn" alt="">
                        </div>
                        <div class=" col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-left">
                            <div class=" delay-01s animated fadeInDown wow animated">
                                {!! $page->description !!}
                                @include('form')
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
@endsection('main')