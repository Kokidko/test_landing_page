<div class="form">
    <form action="/" method="post" id="form">
        <input class="input-text" type="text" name="name"  id="name" value="Your Name *" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
        <input class="input-text" type="text" name="email"  id="email" value="Your E-mail *" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
        <input class="input-text" type="text" name="phone"  id="phone" value="Your Phone *" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
        <input type="hidden" id="csrf" value="{{csrf_token()}}" />
        <input type="hidden" name="pageEmail" id="pageEmail" value="{{$page->email}}">
        <input class="input-btn " type="submit" value="send message">
    </form>
</div>
