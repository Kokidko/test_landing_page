<header id="header_wrapper">
    <div class="container">
        <div class="header_box">
            <div class="logo">
                    <img src="{{ asset('assets/img/logo.png') }}" alt="logo">
            </div>


                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                            <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="main-nav" class="collapse navbar-collapse navStyle">
                        <ul class="nav navbar-nav" id="mainNav">
                            @foreach($pages as $page)
                                <li><a href="#{{$page['slug']}}" class="scroll-link">{{$page['title']}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </nav>


        </div>
    </div>
</header>